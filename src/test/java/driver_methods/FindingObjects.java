package driver_methods;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;

public class FindingObjects {
    WebDriver driver;

    @BeforeEach
    public void driverSetup() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().setSize(new Dimension(1290, 730));
        driver.manage().window().setPosition(new Point(8, 30));
        driver.navigate().to("http://wikipedia.pl/");
    }

    @AfterEach
    public void driverQuit() {
        driver.close();
        driver.quit();
    }

    @Test
    public void findingElementById() {
        driver.findElement(By.id("searchInput"));
        driver.findElement(By.name("search"));
        driver.findElement(By.className("searchButton"));

        List<WebElement> externalClassElements = driver.findElements(By.className("external"));

        WebElement elementWithTwoClasses = null;

        for (WebElement externalClassElement : externalClassElements) {
            String elementClass = externalClassElement.getAttribute("class");
            if (elementClass.equals("external text")) {
                elementWithTwoClasses = externalClassElement;
            }
            Assertions.assertTrue(elementWithTwoClasses != null, "Element was not found");
        }

        int numberOfImages = driver.findElements(By.tagName("img")).size();
    }

    @Test
    public void findingElementByLinkText(){
        driver.findElements(By.linkText("Wikisłownik"));
        driver.findElements(By.partialLinkText("każdy"));

    }

    @Test
    public void findingElementByXpath(){
        driver.navigate().to("https://fakestore.testelka.pl/");
        driver.findElement(By.xpath(".//*[@class='site-main']//ul[contains(@class,'products')]//li[contains(@class, 'product')]"));
        driver.findElement(By.xpath(".//*[@class='site-main']"))
                .findElement(By.xpath(".//ul[contains(@class,'products')]//li[contains(@class, 'product')]"));
    }

    //https://testelka.pl/kurs/selenium/inicjalizacja-driverow/


}
