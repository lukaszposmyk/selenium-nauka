package driver_methods;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class FileUploadExample {
    WebDriver driver;

    @BeforeEach
    public void driverSetup() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().setSize(new Dimension(1295, 730));
        driver.manage().window().setPosition(new Point(10, 40));

        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(15, TimeUnit.SECONDS);
        driver.navigate().to("https://gofile.io/uploadFiles");
    }

    @AfterEach
    public void driverQuit() {
        driver.quit();
    }

    @Test
    public void fileUploadTest() {
        WebElement uploadFileInput = driver.findElement(By.cssSelector("input[type='file']"));
        String fileName = "bug.jpg";
        String path = "C:\\Users\\lukas\\Desktop\\Zdjęcia\\różne\\Wenecja\\" + fileName;
        uploadFileInput.sendKeys(path);

        String actualFileName = driver.findElement(By.cssSelector("table[id='uploadFiles-datatable']>tbody>tr>td:first-child"))
                .getText();
        Assertions.assertEquals(fileName, actualFileName, "Name of uploaded file is different then expected one.");

    }

}
