package driver_methods;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;

public class Frame {
    WebDriver driver;

    @BeforeEach
    public void driverSetup() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().setSize(new Dimension(1290, 730));
        driver.manage().window().setPosition(new Point(8, 30));
        driver.navigate().to("https://www.nasa.gov/topics/history/index.html");
    }

    @AfterEach
    public void driverQuit() {
        driver.close();
        driver.quit();
    }

    @Test
    public void frameExamples() {
        WebElement frame = driver.findElement(By.cssSelector("iframe#twitter-widget-0"));
//        driver.switchTo().frame("twitter-widget-0");
//        driver.switchTo().frame(0);
        driver.switchTo().frame(frame);

        WebElement element = driver.findElement(By.cssSelector("a[data-scribe*='twitter_url']"));
//        driver.switchTo().defaultContent();
        driver.switchTo().parentFrame();
        driver.findElement(By.cssSelector("img[alt='Nasa']")).click();

    }

}
