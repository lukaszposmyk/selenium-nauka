package driver_methods;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class MoreMethodsOnElements {
    WebDriver driver;
    WebDriverWait webDriverWait;
    By windsurfingGroup = By.cssSelector("a[href*='windsurfing']");
    By product = By.cssSelector("li[class*='post-386']");
    By addToCartButton = By.cssSelector("button[name='add-to-cart']");
    By goToCartButton = By.cssSelector("a[title='Zobacz swój koszyk']");

    @BeforeEach
    public void driverSetup() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().window().setPosition(new Point(10, 40));
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
        driver.navigate().to("https://fakestore.testelka.pl/");

        webDriverWait = new WebDriverWait(driver, 10);

        driver.findElement(windsurfingGroup).click();
        driver.findElement(product).click();
        driver.findElement(addToCartButton).click();
        driver.findElement(goToCartButton).click();
    }

    @AfterEach
    public void driverQuit() {
        driver.quit();
    }

    @Test
    public void infoOnElement(){
        WebElement webElement = driver.findElement(By.cssSelector("header[id='masthead']"));
        String text = webElement.getText();
        String attribute = webElement.getAttribute("role");
        String cssValue = webElement.getCssValue("background-color");
        String tagName = webElement.getTagName();
        Point location = webElement.getLocation();
        Dimension size = webElement.getSize();
        Rectangle locationAndSize = webElement.getRect();
        boolean isDisplayed = webElement.isDisplayed();
        boolean isSelected = webElement.isSelected();
        boolean isEnabled = webElement.isEnabled();
    }


}
