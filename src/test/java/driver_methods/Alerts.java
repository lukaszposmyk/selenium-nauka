package driver_methods;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class Alerts {
    WebDriver driver;
    JavascriptExecutor javascriptExecutor;
    WebDriverWait webDriverWait;

    @BeforeEach
    public void driverSetup() {
        WebDriverManager.firefoxdriver().setup();
        driver = new FirefoxDriver();
        driver.manage().window().setSize(new Dimension(1290, 730));
        driver.manage().window().setPosition(new Point(8, 30));
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
        javascriptExecutor = (JavascriptExecutor) driver;
        webDriverWait = new WebDriverWait(driver, 5);
    }

    @AfterEach
    public void driverQuit() {
        driver.quit();
    }

    @Test
    public void promptBoxTest() {
        String javaScript = "prompt('Możesz tutaj coś wpisać:')";
        javascriptExecutor.executeScript(javaScript);
        webDriverWait.until(ExpectedConditions.alertIsPresent());
        String textPrompt = driver.switchTo().alert().getText();
        driver.switchTo().alert().sendKeys("KING");
        driver.switchTo().alert().dismiss();
        javascriptExecutor.executeScript(javaScript);
        driver.switchTo().alert().dismiss();
    }
}