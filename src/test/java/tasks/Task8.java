package tasks;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

public class Task8 {
    WebDriver driver;
    WebDriverWait webDriverWait;

    @BeforeEach
    public void driverSetup() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().window().setPosition(new Point(10, 40));
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
        driver.navigate().to("https://fakestore.testelka.pl/metody-na-elementach/");

        webDriverWait = new WebDriverWait(driver, 10);
    }

    @AfterEach
    public void driverQuit() {
        driver.quit();
    }

    @Test
    public void checkingElementsStateTest() {
        WebElement mainPageButton = driver.findElement(By.cssSelector("input[name='main-page']"));
        WebElement invisibleButton = driver.findElement(By.cssSelector("a[name='sailing']"));
        List<WebElement> yellowElements = driver.findElements(By.cssSelector("a.button"));
        WebElement checkedBox = driver.findElement(By.cssSelector("input[name='selected-checkbox']"));
        WebElement uncheckedBox = driver.findElement(By.cssSelector("input[name='not-selected-checkbox']"));
        WebElement checkedRadio = driver.findElement(By.cssSelector("input[name='selected-radio']"));
        WebElement uncheckedRadio = driver.findElement(By.cssSelector("input[name='not-selected-radio']"));
        List<WebElement> classButtonElements = driver.findElements(By.cssSelector("a.button"));

        assertAll("state",
                () -> assertFalse(mainPageButton.isEnabled(), "'Main page' button is not disable."),
                () -> assertFalse(invisibleButton.isDisplayed(), "'Sailing' button is probably displayed."),
                () -> assertThatButtonsAreYellow(yellowElements),
                () -> assertTrue(checkedBox.isSelected(), "Checkbox is not selected."),
                () -> assertFalse(uncheckedBox.isSelected(), "Radiobutton is not selected."),
                () -> assertTrue(checkedRadio.isSelected(), "Checkbox is probably selected."),
                () -> assertFalse(uncheckedRadio.isSelected(), "Radiobutton is probably selected."),
                () -> assertThatButtonsHaveTagA(classButtonElements)
        );
    }

    public void assertThatButtonsAreYellow(List<WebElement> buttons) {
        for (WebElement yellowElement : buttons) {
            String elementColor = yellowElement.getCssValue("background-color");
            Assertions.assertEquals("rgba(245, 233, 101, 1)", elementColor);
        }
    }

    public void assertThatButtonsHaveTagA(List<WebElement> buttons) {
        for (WebElement classButtonElements : buttons) {
            String elementTagName = classButtonElements.getTagName();
            Assertions.assertEquals("a", elementTagName);
        }
    }


}


