package tasks;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Task1 {
    WebDriver driver;

    @BeforeEach
    public void driverSetup() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().setSize(new Dimension(1280, 720));
    }

    @AfterEach
    public void driverQuit() {
        driver.close();
        driver.quit();
    }

    @Test
    public void testIfLanguageChanged() {

        //1.Przejdź na stronę http://wikipedia.pl
        driver.navigate().to("http://wikipedia.pl");

        //2.Napisz trzy asercje:
        //a.porównaj tytuł strony z oczekiwanym;
        String polishTitleExpected = "Wikipedia, wolna encyklopedia";
        Assertions.assertEquals(polishTitleExpected, driver.getTitle(), "Page title is not: " + polishTitleExpected);

        //b.porównaj URL strony z oczekiwanym;
        String polishUrlExpected = "https://pl.wikipedia.org/wiki/Wikipedia:Strona_g%C5%82%C3%B3wna";
        Assertions.assertEquals(polishUrlExpected, driver.getCurrentUrl(), "Page url is not: " + polishUrlExpected);

        //c.znajdź w konsoli deweloperskiej (F12) w zakładce Elements jakiś fragment źródła strony, który mówi o tym
        //w jakiej wersji językowej jest strona; użyj tego fragmentu źródła do asercji.
        String languagePolish = "lang=\"pl\"";
        Assertions.assertTrue(driver.getPageSource().contains(languagePolish), " Page does not contain 'pl");

        //3. Zmień język strony na hiszpański (By.cssSelector("a[title='hiszpański']")).
        driver.findElement(By.cssSelector("a[title='hiszpański']")).click();

        //4.Napisz trzy asercje:
        //a.porównaj tytuł strony z oczekiwanym;
        String spanishTitleExpected = "Wikipedia, la enciclopedia libre";
        Assertions.assertEquals(spanishTitleExpected, driver.getTitle(), "Page title is not : " + spanishTitleExpected);

        //b.porównaj URL strony z oczekiwanym;
        String spanishUrlExpected = "https://es.wikipedia.org/wiki/Wikipedia:Portada";
        Assertions.assertEquals(spanishUrlExpected, driver.getCurrentUrl(), "Page url is not: " + spanishUrlExpected);

        //c.znajdź w konsoli deweloperskiej (F12) w zakładce Elements jakiś fragment źródła strony, który mówi o tym
        //w jakiej wersji językowej jest strona; użyj tego fragmentu źródła do asercji.
        String languageSpanish = "lang=\"es\"";
        Assertions.assertTrue(driver.getPageSource().contains(languageSpanish), " Page does not contain 'es");
    }

}
