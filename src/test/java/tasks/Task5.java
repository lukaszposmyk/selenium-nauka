package tasks;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class Task5 {
    WebDriver driver;

    @BeforeEach
    public void driverSetup() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().setSize(new Dimension(1295, 730));
        driver.manage().window().setPosition(new Point(10, 40));

        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(15, TimeUnit.SECONDS);
        driver.navigate().to("https://fakestore.testelka.pl/moje-konto/");
    }

    @AfterEach
    public void driverQuit() {
        driver.quit();
    }

    String email = "jas07fsola@gmail.com";
    String emailIncorrect = "incorrect@gmail.com";
    String userName = "jas07fsola";
    String userNameIncorrect = "userNameIncorrect";
    String password = "JasFasola07";
    String passwordIncorrect = "incorrectPassword";
    String blankValue = "";
    String accountWelcomeText;
    String accountWelcomeTextExpected;
    String errorMessage;
    String errorMessageExpected;

    @Test
    public void existentEmailCorrectPasswordTest() {
        accountWelcomeTextExpected = "Witaj " + userName + " (nie jesteś " + userName + "? Wyloguj się)";

        loginToAccount(email, password);
        accountWelcomeText = getAccountWelcomeText();

        Assertions.assertEquals(accountWelcomeTextExpected, accountWelcomeText);
    }

    @Test
    public void existentUserCorrectPasswordTest() {
        accountWelcomeTextExpected = "Witaj " + userName + " (nie jesteś " + userName + "? Wyloguj się)";

        loginToAccount(userName, password);
        accountWelcomeText = getAccountWelcomeText();

        Assertions.assertEquals(accountWelcomeTextExpected, accountWelcomeText);
    }

    @Test
    public void nonexistentEmailCorrectPasswordTest() {
        errorMessageExpected = "Nieznany adres email. Proszę sprawdzić ponownie lub wypróbować swoją nazwę użytkownika.";

        loginToAccount(emailIncorrect, password);
        errorMessage = getErrorMessage();

        Assertions.assertEquals(errorMessageExpected, errorMessage);
    }

    @Test
    public void existentEmailIncorrectPasswordTest() {
        errorMessageExpected = "BŁĄD: Dla adresu email jas07fsola@gmail.com podano nieprawidłowe hasło. Nie pamiętasz hasła?";

        loginToAccount(email, passwordIncorrect);
        errorMessage = getErrorMessage();

        Assertions.assertEquals(errorMessageExpected, errorMessage);
    }

    @Test
    public void nonexistentEmailIncorrectPasswordTest() {
        errorMessageExpected = "Nieznany adres email. Proszę sprawdzić ponownie lub wypróbować swoją nazwę użytkownika.";

        loginToAccount(emailIncorrect, passwordIncorrect);
        errorMessage = getErrorMessage();

        Assertions.assertEquals(errorMessageExpected, errorMessage);
    }

    @Test
    public void nonexistentUserCorrectPasswordTest() {
        errorMessageExpected = "Nieznany użytkownik. Proszę spróbować ponownie lub użyć adresu email.";

        loginToAccount(userNameIncorrect, password);
        errorMessage = getErrorMessage();

        Assertions.assertEquals(errorMessageExpected, errorMessage);
    }

    @Test
    public void existentUserIncorrectPasswordTest() {
        errorMessageExpected = "Błąd: Wprowadzone hasło dla użytkownika " + userName + " jest niepoprawne. Nie pamiętasz hasła?";

        loginToAccount(userName, passwordIncorrect);
        errorMessage = getErrorMessage();

        Assertions.assertEquals(errorMessageExpected, errorMessage);
    }

    @Test
    public void nonexistentUserIncorrectPasswordTest() {
        errorMessageExpected = "Nieznany użytkownik. Proszę spróbować ponownie lub użyć adresu email.";

        loginToAccount(userNameIncorrect, passwordIncorrect);
        errorMessage = getErrorMessage();

        Assertions.assertEquals(errorMessageExpected, errorMessage);
    }


    @Test
    public void blankUserCorrectPasswordTest() {
        errorMessageExpected = "Błąd: Nazwa użytkownika jest wymagana.";

        loginToAccount(blankValue, password);
        errorMessage = getErrorMessage();

        Assertions.assertEquals(errorMessageExpected, errorMessage);
    }

    @Test
    public void correctUserBlankPasswordTest() {
        errorMessageExpected = "Błąd: Hasło jest puste.";

        loginToAccount(userName, blankValue);
        errorMessage = getErrorMessage();

        Assertions.assertEquals(errorMessageExpected, errorMessage);
    }

    @Test
    public void blankUserBlankPasswordTest() {
        errorMessageExpected = "Błąd: Nazwa użytkownika jest wymagana.";

        loginToAccount(blankValue, blankValue);
        errorMessage = getErrorMessage();

        Assertions.assertEquals(errorMessageExpected, errorMessage);
    }

    private void loginToAccount(String user, String password) {
        driver.findElement(By.cssSelector("input[id='username']"))
                .sendKeys(user);
        driver.findElement(By.cssSelector("input[id='password']"))
                .sendKeys(password);
        driver.findElement(By.cssSelector("button[type='submit'][name='login']")).click();
    }

    private String getErrorMessage() {
        return driver.findElement(By.cssSelector("ul[class='woocommerce-error']")).getText();
    }

    private String getAccountWelcomeText() {
        return driver.findElement(By.cssSelector("div[class='woocommerce-MyAccount-content']>p")).getText();
    }
}
