package tasks;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class Task6 {
    WebDriver driver;

    @BeforeEach
    public void driverSetup() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().setSize(new Dimension(1295, 730));
        driver.manage().window().setPosition(new Point(10, 40));

        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(15, TimeUnit.SECONDS);
        driver.navigate().to("https://fakestore.testelka.pl/moje-konto/");
    }

    @AfterEach
    public void driverQuit() {
        driver.quit();
    }

    String password = "JasFasola07";
    String accountWelcomeText;
    String errorMessage;
    String userNameAfterLogin = "jas07fsola";


    @DisplayName("Successful login")
    @ParameterizedTest(name = "User: \"{0}\"")
    @CsvSource({"jas07fsola",
            "jas07fsola@gmail.com"})
    void successfulLogin(String userName) {
        loginToAccount(userName, password);
        accountWelcomeText = getAccountWelcomeText();
        Assertions.assertTrue(accountWelcomeText.contains(userNameAfterLogin),
                "My Account page does not contain correct name. Expected name: " + userNameAfterLogin + " was not found in a string: " + accountWelcomeText);
    }

    @DisplayName("Unsuccessful login")
    @ParameterizedTest(name = "User: \"{0}\" with password: {1}")
    @CsvSource({"incorrect@gmail.com, JasFasola07, Nieznany adres email. Proszę sprawdzić ponownie lub wypróbować swoją nazwę użytkownika.",
            "jas07fsola@gmail.com, incorrectPassword, BŁĄD: Dla adresu email jas07fsola@gmail.com podano nieprawidłowe hasło. Nie pamiętasz hasła?",
            "incorrect@gmail.com, incorrectPassword, Nieznany adres email. Proszę sprawdzić ponownie lub wypróbować swoją nazwę użytkownika.",
            "userNameIncorrect, JasFasola07, Nieznany użytkownik. Proszę spróbować ponownie lub użyć adresu email.",
            "jas07fsola, incorrectPassword, Błąd: Wprowadzone hasło dla użytkownika jas07fsola jest niepoprawne. Nie pamiętasz hasła?",
            "userNameIncorrect, incorrectPassword, Nieznany użytkownik. Proszę spróbować ponownie lub użyć adresu email.",
            "'' ,JasFasola07, Błąd: Nazwa użytkownika jest wymagana.",
            "jas07fsola,'',Błąd: Hasło jest puste.",
            "'','',Błąd: Nazwa użytkownika jest wymagana."})
    void unsuccessfulLogin(String userName, String password, String expectedMessage) {
        loginToAccount(userName, password);
        errorMessage = getErrorMessage();
        Assertions.assertEquals(expectedMessage, errorMessage, "Error message is not correct.");
    }

    private void loginToAccount(String user, String password) {
        driver.findElement(By.cssSelector("input[id='username']"))
                .sendKeys(user);
        driver.findElement(By.cssSelector("input[id='password']"))
                .sendKeys(password);
        driver.findElement(By.cssSelector("button[type='submit'][name='login']")).click();
    }

    private String getErrorMessage() {
        return driver.findElement(By.cssSelector("ul[class='woocommerce-error']")).getText();
    }

    private String getAccountWelcomeText() {
        return driver.findElement(By.cssSelector("div[class='woocommerce-MyAccount-content']>p")).getText();
    }
}
