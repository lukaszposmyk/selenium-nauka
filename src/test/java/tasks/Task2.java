package tasks;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Task2 {
    WebDriver driver;

    @BeforeEach
    public void driverSetup() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().setSize(new Dimension(1280, 720));
        driver.navigate().to("http://wikipedia.pl");
    }

    @AfterEach
    public void driverQuit() {
        driver.close();
        driver.quit();
    }

    @Test
    public void cookiesManage() {
        //1.Pobierz wszystkie ciasteczka i przy pomocy asercji sprawdź, czy jest ich tyle ile powinno.
        Assertions.assertEquals(3, driver.manage().getCookies().size(), "Number of cookies is not what expected.");

        //2.Dodaj swoje ciasteczko i potwierdź asercją, że się dodało.
        Cookie myCoookie = new Cookie("myCookie", "007");
        driver.manage().addCookie(myCoookie);
        Assertions.assertEquals(4, driver.manage().getCookies().size(), "Number of cookies is not what expected.");

        //3.Pobierz swoje ciasteczko i użyj asercji, żeby porównać, że nazwa ciasteczka jest taka, jakiej oczekujesz.
        Assertions.assertNotNull(driver.manage().getCookieNamed("myCookie"), "Cookie doesn't exist.");

        //4.Usuń swoje ciasteczko używając obiektu typu Cookie jako parametru i potwierdź, że zostało usunięte.
        driver.manage().deleteCookie(myCoookie);
        Assertions.assertNull(driver.manage().getCookieNamed("myCookie"), "Cookie exist.");

        //5.Usuń jakieś ciasteczko używając jego nazwy jako parametru i potwierdź, że zostało usunięte.
        driver.manage().deleteCookieNamed("GeoIP");
        Assertions.assertNull(driver.manage().getCookieNamed("GeoIP"), "Cookie exist.");

        //6.Pobierz dowolne już istniejące ciasteczko i użyj asercji, żeby potwierdzić, że domena, ścieżka i ustawienie flagi HTTP jest takie, jak tego oczekujemy.
        Cookie cookie = driver.manage().getCookieNamed("WMF-Last-Access");
        Assertions.assertEquals("pl.wikipedia.org", cookie.getDomain(), "Cookie domain is not what expected");
        Assertions.assertEquals("/", cookie.getPath(), "Cookie path is not what expected");
        Assertions.assertTrue( cookie.isHttpOnly(), "cookie is not HTTP only");
    }
}
