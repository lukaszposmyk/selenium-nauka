package tasks;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class Task11 {
    WebDriver driver;
    JavascriptExecutor javascriptExecutor;
    WebDriverWait webDriverWait;

    @BeforeEach
    public void driverSetup() {
        WebDriverManager.firefoxdriver().setup();
        driver = new FirefoxDriver();
        driver.manage().window().setSize(new Dimension(1290, 730));
        driver.manage().window().setPosition(new Point(8, 30));
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
        driver.navigate().to(" https://jsfiddle.net/nm134se7/");
        javascriptExecutor = (JavascriptExecutor) driver;
        webDriverWait = new WebDriverWait(driver, 5);
        WebElement frame = driver.findElement(By.cssSelector("iframe[name= 'result']"));
        driver.switchTo().frame(frame);
    }

    @AfterEach
    public void driverQuit() {
        driver.quit();
    }

    @Test
    public void confirmBoxOKTest() {
        driver.findElement(By.cssSelector("button[onclick='confirmFunction()']")).click();
        driver.switchTo().alert().accept();
        String actuallMessage = driver.findElement(By.cssSelector("p[id='demo']")).getText();
        Assertions.assertEquals("Wybrana opcja to OK!",actuallMessage, "Message is not correct." );
    }
    @Test
    public void confirmBoxAnulujTest() {
        driver.findElement(By.cssSelector("button[onclick='confirmFunction()']")).click();
        driver.switchTo().alert().dismiss();
        String actuallMessage = driver.findElement(By.cssSelector("p[id='demo']")).getText();
        Assertions.assertEquals("Wybrana opcja to Cancel!",actuallMessage, "Message is not correct." );
    }
    @Test
    public void promptBoxOKTest() {
        driver.findElement(By.cssSelector("button[onclick='promptFunction()']")).click();
        String inputName = "Lukasz Posmyk";
        driver.switchTo().alert().sendKeys(inputName);
        driver.switchTo().alert().accept();
        String actuallMessage = driver.findElement(By.cssSelector("p#prompt-demo")).getText();
        Assertions.assertEquals("Cześć "+ inputName + "! Jak leci?",actuallMessage, "Message is not correct." );
    }

    @Test
    public void promptBoxAnulujTest() {
        driver.findElement(By.cssSelector("button[onclick='promptFunction()']")).click();
//        String inputName = "Lukasz Posmyk";
//        driver.switchTo().alert().sendKeys(inputName);
        driver.switchTo().alert().dismiss();
        String actuallMessage = driver.findElement(By.cssSelector("p#prompt-demo")).getText();
        Assertions.assertEquals("Użytkownik anulował akcję.",actuallMessage, "Message is not correct." );
    }

}
