package tasks;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Task3 {
    WebDriver driver;

    @BeforeEach
    public void driverSetup() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.navigate().to("http://wikipedia.pl");
    }

    @AfterEach
    public void driverQuit() {
        driver.close();
        driver.quit();
    }

    @Test
    public void windowManage() {
        //Ustaw rozmiar okna przeglądarki na 854×480.
        Dimension size = new Dimension(854, 480);
        driver.manage().window().setSize(size);

        //Ustaw pozycję okna przeglądarki na 445×30.
        Point position = new Point(445, 30);
        driver.manage().window().setPosition(position);

        //Pobierz rozmiar okna i wykonaj asercję.
        Dimension sizeAfterChange = driver.manage().window().getSize();
        Assertions.assertEquals(size, sizeAfterChange, "Size of the window is not: " + size.width + " " + size.height);

        //Pobierz pozycję okna i wykonaj asercję.
        Point positionAfterChange = driver.manage().window().getPosition();
        Assertions.assertEquals(position, positionAfterChange, "Position of the window is not: " + size.width + " " + size.height);

        //Zmaksymalizuj okno przeglądarki.
        driver.manage().window().maximize();

        //Ustaw przeglądarkę na fullscreen.
        driver.manage().window().fullscreen();

    }
}
