package tasks;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class Task9 {
    WebDriver driver;
    WebDriverWait webDriverWait;
    By demoStoreBar = By.cssSelector("a[class*='dismiss-link']");

    @BeforeEach
    public void driverSetup() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
//        driver.manage().window().setSize(new Dimension(1295, 760));
        driver.manage().window().maximize();
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
        driver.navigate().to("https://fakestore.testelka.pl/cwiczenia-z-ramek/");
        driver.findElement(demoStoreBar).click();

        webDriverWait = new WebDriverWait(driver, 10);
    }

    @AfterEach
    public void driverQuit() {
        driver.quit();
    }

    @Test
    public void mainPageFirstButtonEnableTest() {
        driver.switchTo().frame("main-frame");
        WebElement mainPageButton = driver.findElement(By.cssSelector("input[value='Strona główna']"));
        Assertions.assertFalse(mainPageButton.isEnabled());
    }

    @Test
    public void imageLinkToMainPageTest() {
        driver.switchTo().frame("main-frame")
                .switchTo().frame("image");

        WebElement img = driver.findElement(By.xpath(".//img[@alt='Wakacje']/.."));
        Assertions.assertEquals("https://fakestore.testelka.pl/", img.getAttribute("href"),
                "The click into image doesn't link to main page ");
    }

    @Test
    public void mainPageLastButtonEnableTest() {
        driver.switchTo().frame("main-frame")
                .switchTo().frame("image")
                .switchTo().frame(0);

        WebElement mainPageButton = driver.findElement(By.cssSelector("a.button"));
        Assertions.assertTrue(mainPageButton.isEnabled(), "The button is disable!");
    }

    @Test
    public void logoVisibilityTest() throws InterruptedException {
        driver.switchTo().frame("main-frame")
                .switchTo().frame("image")
                .switchTo().frame(0);

        WebElement mainPageButton = driver.findElement(By.cssSelector("a.button"));
        mainPageButton.click();

        WebElement wpinaczkaButton = driver.findElement(By.cssSelector("img[alt='Wspinaczka']"));
        wpinaczkaButton.click();

        driver.switchTo().parentFrame()
                .switchTo().parentFrame();

        WebElement climbingButton = driver.findElement(By.cssSelector("a[name='climbing']"));
        climbingButton.click();

        WebElement logoImage = driver.findElement(By.cssSelector("img[class='custom-logo']"));
        Assertions.assertTrue(logoImage.isDisplayed(), "Logo is not displayed!");
    }
}
