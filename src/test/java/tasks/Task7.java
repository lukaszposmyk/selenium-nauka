package tasks;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class Task7 {
    WebDriver driver;
    WebDriverWait webDriverWait;
    By windsurfingGroup = By.cssSelector("a[href*='windsurfing']");
    By product = By.cssSelector("li[class*='post-386']");
    By addToCartButton = By.cssSelector("button[name='add-to-cart']");
    By goToCartButton = By.cssSelector("a[title='Zobacz swój koszyk']");
    By removeCoupon = By.cssSelector("a[data-coupon='10procent']");
    String correctCoupon = "10procent";
    String incorrectCoupon = "incorrectcoupon";

    @BeforeEach
    public void driverSetup() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().window().setPosition(new Point(10, 40));
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
        driver.navigate().to("https://fakestore.testelka.pl/");

        webDriverWait = new WebDriverWait(driver, 10);

        driver.findElement(windsurfingGroup).click();
        driver.findElement(product).click();
        driver.findElement(addToCartButton).click();
        driver.findElement(goToCartButton).click();
    }

    @AfterEach
    public void driverQuit() {
        driver.quit();
    }

    @Test
    public void emptyCouponTest() {
        applyCoupon("");
        Assertions.assertEquals("Proszę wpisać kod kuponu.", getAlert(), "Alert message was not what expected.");
    }

    @Test
    public void incorrectCouponTest() {
        applyCoupon(incorrectCoupon);
        Assertions.assertEquals("Kupon \"" + incorrectCoupon + "\" nie istnieje!", getAlert(), "Alert message was not what expected.");
    }

    @Test
    public void correctCouponTest() {
        applyCoupon(correctCoupon);
        Assertions.assertEquals("Kupon został pomyślnie użyty.", getAlert(), "Alert message was not what expected.");
    }

    @Test
    public void addingCouponWhenAlreadyAppliedTest() {
        applyCoupon(correctCoupon);
        waitForProcessingEnd();
        applyCoupon(correctCoupon);
        waitForProcessingEnd();
        Assertions.assertEquals("Kupon został zastosowany!", getAlert(), "Alert message was not what expected.");
    }

    @Test
    public void removingCouponTest() {
        applyCoupon(correctCoupon);
        waitForProcessingEnd();
        webDriverWait.until(ExpectedConditions.elementToBeClickable(removeCoupon)).click();
        waitForProcessingEnd();
        Assertions.assertEquals("Kupon został usunięty.", getAlert(), "Alert message was not what expected.");

    }

    private void applyCoupon(String coupon) {
        By couponCodeField = By.cssSelector("input[id='coupon_code']");
        By addCouponButton = By.cssSelector("button[value='Zastosuj kupon'");

        driver.findElement(couponCodeField).sendKeys(coupon);
        driver.findElement(addCouponButton).click();
    }

    private void waitForProcessingEnd() {
        By blockerUI = By.cssSelector("div[class='blockUI']");
        webDriverWait.until(ExpectedConditions.numberOfElementsToBeMoreThan(blockerUI, 0));
        webDriverWait.until(ExpectedConditions.numberOfElementsToBe(blockerUI, 0));
    }

    private String getAlert() {
        By alertMessage = By.cssSelector("[role='alert']");
        return webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(alertMessage)).getText();

    }


}
