package tasks;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Task4 {
    WebDriver driver;

    @BeforeEach
    public void driverSetup() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.navigate().to("http://fakestore.testelka.pl/moje-konto/");
    }

    @AfterEach
    public void driverQuit() {
        driver.close();
        driver.quit();
    }

    @Test
    public void findingElementsOnWebsite() {
        driver.findElement(By.id("woocommerce-product-search-field-0"));
        driver.findElement(By.name("s"));
        driver.findElements(By.className("search-field"));

        driver.findElements(By.name("username"));

        driver.findElement(By.name("password"));

        driver.findElement(By.name("login"));

        driver.findElement(By.name("rememberme"));

        driver.findElement(By.linkText("Nie pamiętasz hasła?"));

        driver.findElement(By.linkText("Żeglarstwo"));
    }
}
