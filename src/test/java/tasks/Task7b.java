package tasks;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;


public class Task7b {
    WebDriver driver;
    WebDriverWait webDriverWait;
    By windsurfingGroup = By.cssSelector("a[href*='windsurfing']");
    By product = By.cssSelector("li[class*='post-386']");
    By addToCartButton = By.cssSelector("button[name='add-to-cart']");
    By goToCartButton = By.cssSelector("a[title='Zobacz swój koszyk']");
    By removeCoupon = By.cssSelector("a[data-coupon='10procent']");
    String correctCoupon = "10procent";
    String incorrectCoupon = "incorrectcoupon";

    @BeforeEach
    public void driverSetup() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().window().setPosition(new Point(10, 40));
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
        driver.navigate().to("https://fakestore.testelka.pl/");

        webDriverWait = new WebDriverWait(driver, 10);

        driver.findElement(windsurfingGroup).click();
        driver.findElement(product).click();
        driver.findElement(addToCartButton).click();
        driver.findElement(goToCartButton).click();
    }

    @AfterEach
    public void driverQuit() {
        driver.quit();
    }

    @DisplayName("Simple coupon test")
    @ParameterizedTest(name = "Coupon: \"{0}\"")
    @CsvSource({"'', Proszę wpisać kod kuponu.",
            "incorrectcoupon, Kupon \"incorrectcoupon\" nie istnieje!",
            "10procent, Kupon został pomyślnie użyty."})
    void simpleAddingCouponTest(String couponName, String expectedAlert) {
        applyCoupon(couponName);
        Assertions.assertEquals(expectedAlert, getAlert(), "Alert message was not what expected.");
    }

    @Test
    public void addingCouponWhenAlreadyAppliedTest() {
        applyCoupon(correctCoupon);
        waitForProcessingEnd();
        applyCoupon(correctCoupon);
        waitForProcessingEnd();
        Assertions.assertEquals("Kupon został zastosowany!", getAlert(), "Alert message was not what expected.");
    }

    @Test
    public void removingCouponTest() {
        applyCoupon(correctCoupon);
        waitForProcessingEnd();
        webDriverWait.until(ExpectedConditions.elementToBeClickable(removeCoupon)).click();
        waitForProcessingEnd();
        Assertions.assertEquals("Kupon został usunięty.", getAlert(), "Alert message was not what expected.");

    }

    private void applyCoupon(String coupon) {
        By couponCodeField = By.cssSelector("input[id='coupon_code']");
        By addCouponButton = By.cssSelector("button[value='Zastosuj kupon'");

        driver.findElement(couponCodeField).sendKeys(coupon);
        driver.findElement(addCouponButton).click();
    }

    private void waitForProcessingEnd() {
        By blockerUI = By.cssSelector("div[class='blockUI']");
        webDriverWait.until(ExpectedConditions.numberOfElementsToBeMoreThan(blockerUI, 0));
        webDriverWait.until(ExpectedConditions.numberOfElementsToBe(blockerUI, 0));
    }

    private String getAlert() {
        By alertMessage = By.cssSelector("[role='alert']");
        return webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(alertMessage)).getText();

    }
}


