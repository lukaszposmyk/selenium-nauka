package tasks;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Task {
    WebDriver driver;

    @BeforeEach
    public void driverSetup() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().setSize(new Dimension(1280, 720));
    }

    @AfterEach
    public void driverQuit() {
        //7.Zamknij okno przeglądarki.
        driver.close();
        //8.Zamknij sesję.
        driver.quit();
    }

    @Test
    public void navigationTest() {
        //1. Otworzy stronę główną Wikipedii.
        driver.navigate().to("https://pl.wikipedia.org/wiki/Wikipedia:Strona_g%C5%82%C3%B3wna");

        //2.Następnie otworzy stronę główną Nasa.
        driver.navigate().to("https://www.nasa.gov/");

        //3.Cofnie się do strony Wikipedii (używając nawigacji wstecz).
        driver.navigate().back();

        //4.Potwierdź, że driver jest na stronie Wikipedii: porównaj (Assertions.assertEquals()) tytuł strony z oczekiwanym.
        String actualTitleWikipedia = driver.getTitle();
        String expectedTitleWikipedia = "Wikipedia, wolna encyklopedia";
        Assertions.assertEquals(actualTitleWikipedia, expectedTitleWikipedia, "The title of the page is not " + actualTitleWikipedia);

        //5.Przejdź do strony Nasa (używając nawigacji naprzód).
        driver.navigate().forward();

        //6.Potwierdź, że driver jest na stronie Nasa: porównaj tytuł strony z oczekiwanym.
        String actualTitleNasa = driver.getTitle();
        String expectedTitleNasa = "NASA's SpaceX Crew-1 Mission | NASA";
        Assertions.assertEquals(actualTitleNasa, expectedTitleNasa, "The title of the page is not " + actualTitleNasa);

    }
}
