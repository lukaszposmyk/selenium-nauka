package tasks;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class Task10 {
    WebDriver driver;
    WebDriverWait webDriverWait;

    @BeforeEach
    public void driverSetup() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().window().setPosition(new Point(10, 40));
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
        driver.navigate().to("https://fakestore.testelka.pl/product/zmien-swoja-sylwetke-yoga-na-malcie/");
        webDriverWait = new WebDriverWait(driver, 10);
    }

    @AfterEach
    public void driverQuit() {
        driver.quit();
    }

    @Test
    public void scrollingMenuAppearsTest() {
        WebElement element = driver.findElement(By.cssSelector("a[class*='woocommerce-LoopProduct-link']"));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView()", element);
        List<WebElement> webElements = driver.findElements(By.cssSelector("section[class*='storefront-sticky-add-to-cart--slideInDown']"));
        Assertions.assertTrue(webElements.size() == 1);


    }

}
